<!DOCTYPE html>
<html>
<head>
  <title>Tugas Akhir</title>
  <style type="text/css">
  *{ padding: 0px; margin: 0px;  }

  body {width:1000px; margin: auto; }

  header {background-color: #000080; font-family: verdana, arial, arial black}
  header h1{color: #FDF5E6;padding: 20px; padding-bottom: 15px; padding-left: 90px; text-align: center; text-align: center;}
  header img{ padding-top: 0px;}

  menu {background-color: #2197ff; height: 50px;}
  menu ul {list-style: none; line-height: 50px}
  menu ul li { float: left; width: 20%; text-align: center;}
  menu ul li a{color: white; text-decoration: none; font-family: verdana; display: block;}
  menu ul li a:hover{ background-color: green; color: black }

  .waktu{ padding-left: 10px; padding-bottom: 10px; color: white;}

  .bela img{ margin-top: 1px; }
  .pres img{ padding-right: 90px; }

  #link{ font-size: medium; margin-bottom: 50px; padding-bottom: 49px;  margin-top: 10px;}
  article tr {color: #000000 }
  article li {font-family: verdana; margin-left: 25px;}
  article p {font-family: verdana;}
  article h2 {font-family: verdana;font-style: italic;}

  .diary{ border: 1px solid black; height: 250px; width: 220px; padding-left: 10px; padding-bottom: 10px;
      padding-top: 20px; }
  .diary h3{ margin-top: 20px; }
  .diary input[type="button"]{
    margin : 30px;
    border: none;
    outline: none;
    height: 30px;
    width: 80px;
    background: #2197ff;
    color: #fff;
    font-size: 10px;
    border-radius: 10px;
  }
  .diary  input[type="button"]:hover
  {
    cursor: pointer;
    background: #63ff21;
    color: #000;
  }
  .tamu{ border: 1px solid black; height: 390px; width: 220px; padding-left: 10px;margin-top: 50px; 
       padding-top: 20px; margin-bottom: 250px;}
  .tamu form{ padding-left: 20px;  }
  .tamu h3{ margin-top: 15px; }
  .tamu input[type="submit"]{
    margin : 6px;
    border: none;
    outline: none;
    height: 30px;
    width: 54px;
    background: #2197ff;
    color: #fff;
    font-size: 15px;
    border-radius: 10px;
  }
  .tamu  input[type="submit"]:hover
  {
    cursor: pointer;
    background: #63ff21;
    color: #000;
  }
  .tamu input[type="reset"]{
    margin : 6px;
    border: none;
    outline: none;
    height: 30px;
    width: 54px;
    background: #2197ff;
    color: #fff;
    font-size: 15px;
    border-radius: 10px;
  }
  .tamu  input[type="reset"]:hover
  {
    cursor: pointer;
    background: #63ff21;
    color: #000;
  }

  .bio{  padding-left:30px; padding-top: 20px;}
  .bio ul li{ margin-top: 10px; list-style: none;}

  .pend{ padding-left: 30px; margin-top: 10px; }
  .pend ul li{ margin-top: 10px; }
  .pres{padding-left: 30px;}
  .pres img{ margin-top: 5px; }

  .hobby{ padding-left: 30px; }

  .history{ padding-left: 30px; }
  .history ul li{ margin-top: 10px; }

  .ilmiah{ padding-left: 30px; }
  .ilmiah ul li{margin-top: 10px;}

  footer {background-color: #000080; }
  footer {text-align: center;color: white}
     
  </style>
</head>
<body>
  <div>
    <header>
      <table>
        <tr>
          <td width="90%">
            <h1> HELLO I'M BELLA</h1>
            <br>
            <div class="waktu">
            <script language="javascript">
              <!--
                var tanggal= new Date();
                var kode_hari= tanggal.getDay();
                var nama_hari="";
                if(kode_hari==0) nama_hari="Minggu";
                else if(kode_hari==1) nama_hari="Senin";
                 else if(kode_hari==2) nama_hari="Selasa";
                  else if(kode_hari==3) nama_hari="Rabu";
                   else if(kode_hari==4) nama_hari="Kamis";
                    else if(kode_hari==5) nama_hari="Jumat";
                      else
                        nama_hari="Sabtu";
                document.write("Hari "+nama_hari);
                document.write(", tanggal " +tanggal.getDate()+ "/" +(tanggal.getMonth()+1) +"/"+tanggal.getFullYear());
                document.write("<br>")
                document.write("Pukul "+tanggal.getHours()+"." +tanggal.getMinutes());
              -->
            </script>
            </div>
          </td>
        </tr>
      </table>
    </header>
  </div>
  <menu>
    <ul>
      <li><a href="menu12.php">MENU</a></li>
      <li><a href="Pert12.php">HOME</a></li>
      <li><a href="about.php">ABOUT ME</a></li>
      <li><a href="kontak.php">CONTACT</a></li>
      <li><a href="">CATEGORIES</a></li>
    </ul>
  </menu>
  <div>
    <article>
      <table border="2" cellpadding="20">
        
          <td width="20%">
          <div class="bela"><img src="labella.png" width="230" height="200"></div>

             <br> <br> <br>
            <p id="link">
            <center><p><b>POPULAR POST</b></p></center><br>
            <li><a href=""> Pengertian HTML </a></li>
            <li><a href=""> Syntax HTML </a></li>
            <li><a href=""> Tag Paragraf </a></li>
            <li><a href=""> Tag Atribut </a></li>
            <li><a href=""> Read More </a></li>
            <li><a href=""> Karya Ilmiah </a></li>
            <br> <br> <br>
            <p>
            <div class="diary">
              <center><p><h3><b>DIARY</b></h3></p></center><br>
              <img src="diary.jpg" width="140" height="70" align="left"><br><br>
              <center><input type="button" value="READ MORE"></center>
            </div><br>
            <div class="tamu">
              <table>
                <form action="" method="post" name="input">
                <center><p><b> <h3>FORM BUKU TAMU</h3> <b/></p></center><br>
                 Nama : <input type="text" name="nama"><br>
                 Email : <input type="text" name="email"><br>
                 Pesan :<br><textarea type="textarea" name="pesan"></textarea><br>
                <center><br>
                  <input type="submit"  value="simpan" name="input">
                  <input type="reset" value="Batal" name="batal">
                </center>
              </form>
                <?php
                  if(isset($_POST['input'])){
                    $nama=$_POST['nama'];
                    echo"Nama :$nama<br>";
                    $email=$_POST['email'];
                    echo"Email :$email<br>";
                    $pesan=$_POST['pesan'];
                    echo"Pesan : $pesan<br>";
                  }
                ?>
              </table>
            </div>
            </p>
          <td width="900">
            <div class="bio">
              <table>
                <p><h2>BIODATA </h2></p>
                <tr>
                  <td><img src="logo.jpg" width="100" height="100">
                    <td>
                      <ul>
                        <li>
                          <?php
                            $nama='Bela Meilani';
                            $nim="1900018365";
                            $kelas='G';
                            $umur='19 Tahun';
                           
                            echo"Nama : $nama<br>";
                            echo"Nim  : ".$nim."<br>";
                            echo"Kelas : $kelas<br>";
                            echo"Umur : $umur<br>";
                          ?>
                        </li>
                      </ul>
                    </td>
                  </td>
                </tr>
              </table><br><br>
            <br>
            </div>
            <p>
            <div class="pend">
              <h3> HISTORY PENDIDIKAN</h3><br>
              <ul>
                <li>SDN Pedasong 2</li>
                <li>SMPN 3 Kroya</li>
                <li>SMAN 1 Kroya</li>
              </ul>
            </div>
            </p><br><br>
            <div class="pres">
               <h3> PRESTASI</h3>
              <table>
                <tr>
                  <td><img src="buku.jpg" width="100" height="100"><br>JUARA 1 CERPEN</td>
                </tr>
              </table>
            </div><br><br>
            <div class="hobby">
              <h3> HOBBY </h3>
                  <img src="pensil.jpg" width="200" height="80"><center><em><h4>MY HOBBY IS WRITING</h4></em></center><br><br>
                  <p>
                    <h3>Serunya Mempunyai Hobi Menulis</h3><br>
                    <p>
                      Hobi Menulis?<br><br>
                      Apa sih yang orang pikirkan tentang seseorang yang memiliki hobi menulis?<br>
                      Ah, tukang ngarang.<br>
                      Ah, tukang ngekhayal.<br>
                      Ah, serius mulu.<br>
                      Ah, bla bla bla...<br><br>
                      Hei, jangan pandang orang yang punya hobi menulis dengan sebelah mata lah, gak penting lah. Para Penulis itu pasti berawal dari Hobi-nya dalam menulis, hingga menghasilkan sebuah karya yang banyak dicintai oleh masyarakat.<br><br>

                      Nah, terus gimana si Hobi Menulis tetapi belum mendapatkan hasil?<br><br>
                      Pertama, you guys must to know it.<br>
                      Menulis itu tidak mudah.<br>
                      Menulis itu butuh konsentrasi tinggi.<br>
                      Menulis itu butuh pemikiran yang keras.<br>
                      Kenapa?<br><br>

                      Lah, nulis (ngetik) sambil otak berkeliaran kesana-kemari nyari kata yang tepat dan alur yang pas biar nyambung emang gampang? BIG NO!<br><br>

                      Dimana mereka harus ngetik sambil mikir kata yang pas dengan memperhatikan EYD juga menjelaskan situasi-kondisi si tokoh dengan singkat padat dan jelas.<br><br>

                      Ketik - Baca Ulang - Hapus - Ubah - Ketik<br><br>

                      Siklusnya begituuuuu terus...


                    </p>
                  </p> 
            </div><br><br>
            <div class="history">
              <h3> HISTORY</h3>
              <ul>
                <li>Pekerjaan : Analysis</li>
                <li>Pengalaman : Join Competition Desain Web</li>
              </ul>
            </div>
            <br><br>
            <div class="ilmiah">
              <h3>KARYA ILMIAH</h3>
              <ul>
                <li>Pemanfaatan Kembali Sampah</li>
                <li>Bahaya Internet Bagi Remaja</li>
                <li><a href="">Read more.....</a></li>
                <br>
              </ul>
            </div>
          </td>
          </div>
      </table>
    </article>
  </div>
  <footer>
    <p>&copy; copyright Bela Meilani</p>
  </footer>
</body>
</html>
